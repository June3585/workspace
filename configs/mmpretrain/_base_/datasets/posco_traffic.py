# dataset settings
dataset_type = 'PoscoTraffic'
data_preprocessor = dict(
    num_classes=3,
    # RGB format normalization parameters
    mean=[0., 0., 0.],
    std=[1., 1., 1.],
    # convert image from BGR to RGB
    # to_rgb=True,
)

bgr_mean = data_preprocessor['mean'][::-1]
bgr_std = data_preprocessor['std'][::-1]

train_pipeline = [
    dict(type='LoadImageFromFile'),
    dict(type='Resize', scale=224),
    dict(type='RandomFlip', prob=0.5, direction='horizontal'),
    dict(
        type='AutoAugment',
        policies='imagenet',
        hparams=dict(pad_val=[round(x) for x in bgr_mean])),
    dict(
        type='RandomErasing',
        erase_prob=0.2,
        mode='rand',
        min_area_ratio=0.02,
        max_area_ratio=0.10,
        fill_color=bgr_mean,
        fill_std=bgr_std),
    dict(type='PackInputs'),
]

test_pipeline = [
    dict(type='LoadImageFromFile'),
    dict(type='Resize', scale=224),
    dict(type='PackInputs'),
]

train_dataloader = dict(
    batch_size=256,
    num_workers=16,
    dataset=dict(
        type=dataset_type,
        data_root='datasets/secondary_traffic',
        ann_file='layout/secondary_traffic.txt',
        data_prefix=dict(img_path='crop', ann_path='cls'),
        pipeline=train_pipeline),
    sampler=dict(type='DefaultSampler', shuffle=True),
)

val_dataloader = dict(
    batch_size=256,
    num_workers=16,
    dataset=dict(
        type=dataset_type,
        data_root='datasets/secondary_traffic',
        ann_file='layout/secondary_traffic.txt',
        data_prefix=dict(img_path='crop', ann_path='cls'),
        pipeline=test_pipeline),
    sampler=dict(type='DefaultSampler', shuffle=False),
)
val_evaluator = dict(type='Accuracy', topk=(1, 3))

# If you want standard test, please manually configure the test dataset
test_dataloader = val_dataloader
test_evaluator = val_evaluator
