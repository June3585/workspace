# Refers to https://pytorch.org/blog/ml-models-torchvision-v0.9/#classification

_base_ = [
    '../../_base_/datasets/posco_coupler.py',
    '../../_base_/models/mobilenet_v3_small_imagenet.py',
    '../../_base_/default_runtime.py',
]

model = dict(
    head=dict(
        num_classes=2,
        topk=(1, 2)))

# schedule settings
optim_wrapper = dict(
    optimizer=dict(
        type='AdamW',
        lr=0.02,
        weight_decay=0.025,
        eps=1e-8,
        betas=(0.9, 0.999)),
    paramwise_cfg=dict(
        norm_decay_mult=0.0,
        bias_decay_mult=0.0,
        custom_keys={
            '.attention_biases': dict(decay_mult=0.0),
        }))

param_scheduler = [
    dict(
        type='LinearLR',
        start_factor=1e-6 / 0.02,
        by_epoch=True,
        begin=0,
        end=20,
        convert_to_iter_based=True),
    dict(
        type='CosineAnnealingLR',
        by_epoch=True,
        begin=20,
        end=300,
        eta_min=1e-6,
        convert_to_iter_based=True)]

train_cfg = dict(by_epoch=True, max_epochs=300, val_interval=5)
val_cfg = dict()
test_cfg = dict()

# NOTE: `auto_scale_lr` is for automatically scaling LR
# based on the actual training batch size.
# base_batch_size = (8 GPUs) x (128 samples per GPU)
auto_scale_lr = dict(base_batch_size=1024)
