import os

from mmengine.runner import find_latest_checkpoint

'''
Example:
model_cfg='./configs/mmdet/yolox/coco/yolox_s_8x8_coco.py'

deploy_cfg='./configs/mmdeploy/mmdet/detection/detection_tensorrt_static-416x416.py'
deploy_cfg='./configs/mmdeploy/mmdet/detection/detection_tensorrt-fp16_static-416x416.py'
deploy_cfg='./configs/mmdeploy/mmdet/detection/detection_tensorrt-int8_static-416x416.py'

'''

# config files
model_cfg='./configs/mmdet/yolox/coco/yolox_s_8x8_coco.py'

deploy_cfg='./configs/mmdeploy/mmdet/detection/detection_tensorrt_static-416x416.py'

# third_party variables
third_party = '../third_party/openmmlab'

# config variables
_, codebase, network, dataset, model = os.path.normpath(model_cfg).split(os.sep)

model_name = os.path.splitext(model)[0]
model_dir = os.path.join('./models', codebase, network, dataset, model_name)
model_pth = find_latest_checkpoint(model_dir)
model_pkl = os.path.join(model_dir, 'result.pkl')

# distributed computing variables
distributed = True
nnodes = 1
node_rank = 0
nproc_per_node = 2
master_addr = "127.0.0.1"
master_port = 29500

