import sys
import subprocess

from configs import *

# deploy
subprocess.run([
    'python',
    f'{third_party}/mmdeploy/tools/deploy.py', 
    f'{deploy_cfg}',
    f'{model_cfg}',
    f'{model_pth}',
    f'{third_party}/codebase/mmdetection/demo/demo.jpg',
    '--work-dir', f'{model_dir}',
    '--device', 'cuda',
    '--dump-info',
])

#