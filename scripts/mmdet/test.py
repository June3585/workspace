import sys
import subprocess

from torch.distributed import launch as distributed_launch
from configs import *

# test
if distributed:
    sys.argv = [
        sys.argv[0], 
        f'--nnodes={nnodes}', 
        f'--node_rank={node_rank}', 
        f'--nproc_per_node={nproc_per_node}', 
        f'--master_addr={master_addr}', 
        f'--master_port={master_port}',
        f'{third_party}/codebase/mmdetection/tools/test.py', 
        f'{model_cfg}',
        f'{model_pth}',
        '--work-dir', f'{model_dir}',
        f'--out={model_pkl}',
        '--launcher=pytorch',
    ]
    distributed_launch.main()
else:
    subprocess.run([
        'python',
        f'{third_party}/codebase/mmdetection/tools/test.py', 
        f'{model_cfg}',
        f'{model_pth}',
        '--work-dir', f'{model_dir}',
        f'--out={model_pkl}',
    ])

    # print('=======================================================')

    # subprocess.run([
    #     'python',
    #     'tools/mmdet/analysis/analyze_results.py', 
    #     f'{model_cfg}',
    #     f'{model_pkl}',
    #     f'{model_dir}',
    # ])

    # print('=======================================================')

    # subprocess.run([
    #     'python', 
    #     'tools/mmdet/analysis/confusion_matrix.py', 
    #     f'{model_cfg}',
    #     f'{model_pkl}',
    #     f'{model_dir}',
    # ])

    # print('=======================================================')

    # # subprocess.run([
    # #     'python',
    # #     'tools/mmdet/analysis/benchmark.py',  
    # #     f'{model_cfg}',
    # #     '--checkpoint', f'{model_pth}',
    # #     '--work-dir', f'{model_dir}',
    # #     '--launcher pytorch '
    # # ])

    # print('=======================================================')

    # subprocess.run([
    #     'python',
    #     'tools/mmdet/analysis/visualize_featmap.py',  
    #     '../3rdparty/openmmlab/codebase/mmdetection/demo/demo.jpg ',
    #     f'{model_cfg}',
    #     f'{model_pth}',
    #     '--out-dir', f'{model_dir}',
    #     '--target-layers', 'backbone',
    #     '--channel-reduction', 'select_max ',
    # ])

    # print('=======================================================')

    # subprocess.run([
    #     'python',
    #     'tools/mmdet/analysis/boxam_vis_demo.py',  
    #     '../3rdparty/openmmlab/codebase/mmdetection/demo/demo.jpg ',
    #     f'{model_cfg}',
    #     f'{model_pth}',
    #     '--out-dir', f'{model_dir}',
    #     '--target-layers', 'neck.out_convs[1]',
    #     '--norm ',
    # ])

    # print('=======================================================')

    # # subprocess.run([
    # #     'python',
    # #     'tools/mmdet/analysis/dataset_analysis.py',  
    # #     f'{model_cfg}',
    # # ])
