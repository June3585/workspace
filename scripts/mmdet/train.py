import sys
import os
import subprocess

from torch.distributed import launch as distributed_launch
from configs import *

# train
if distributed:
    sys.argv = [
        sys.argv[0], 
        f'--nnodes={nnodes}', 
        f'--node_rank={node_rank}', 
        f'--nproc_per_node={nproc_per_node}', 
        f'--master_addr={master_addr}', 
        f'--master_port={master_port}',
        f'{third_party}/codebase/mmdetection/tools/train.py', 
        f'{model_cfg}',
        '--work-dir', f'{model_dir}',
        '--launcher=pytorch', 
    ]
    distributed_launch.main()
else:
    subprocess.run([
        'python',
        f'{third_party}/codebase/mmdetection/tools/train.py', 
        f'{model_cfg}',
        '--work-dir', f'{model_dir}',
    ])

#