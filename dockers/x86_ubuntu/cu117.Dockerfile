FROM nvcr.io/nvidia/pytorch:22.08-py3


# ─────────────────────────────────────────────────────────────────────────────
# WORKSPACE
# ─────────────────────────────────────────────────────────────────────────────
ENV ROOT=/root
ENV WORKSPACE_PATH=${ROOT}/workspace
ENV THIRDPARTY_PATH=${ROOT}/third_party
ENV OPENMMLAB_PATH=${THIRDPARTY_PATH}/openmmlab
ENV BACKEND_PATH=${THIRDPARTY_PATH}/backend


# ─────────────────────────────────────────────────────────────────────────────
# PYTHON LIBRARIES
# ─────────────────────────────────────────────────────────────────────────────
# only install when using 22.08 images, because defualt tensorboard is required
# 3.9.2 <= protobuf < 3.20, 
RUN pip install tensorboard==2.14


# ─────────────────────────────────────────────────────────────────────────────
# OPENMMLAB CORE
# ─────────────────────────────────────────────────────────────────────────────
WORKDIR ${OPENMMLAB_PATH}


# "mmengine"
RUN git clone -b v0.8.4 https://github.com/open-mmlab/mmengine.git mmengine \
    && cd mmengine \
    && pip install --no-cache-dir -e .

# "mmcv"
RUN git clone -b v2.0.1 https://github.com/open-mmlab/mmcv.git mmcv \
    && cd mmcv \
    && MMCV_WITH_OPS=1 FORCE_CUDA=1 pip install --no-cache-dir -e .

# Verify the installation TODO: DELETED
RUN python -c 'from mmengine.utils.dl_utils import collect_env;print(collect_env())'


# ─────────────────────────────────────────────────────────────────────────────
# OPENMMLAB CODEBASE
# ─────────────────────────────────────────────────────────────────────────────
WORKDIR ${OPENMMLAB_PATH}/codebase

# "mmpretrain"
RUN git clone -b main https://github.com/open-mmlab/mmpretrain.git mmpretrain \
    && cd mmpretrain \
    && pip install --no-cache-dir -e .

# "mmdetection"
RUN git clone -b main https://github.com/open-mmlab/mmdetection.git mmdetection \
    && cd mmdetection \
    && pip install --no-cache-dir -e .

# "mmagic"
RUN git clone -b main https://github.com/open-mmlab/mmagic.git mmagic \
    && cd mmdetection \
    && pip install --no-cache-dir -e .

# "mmrazor"
RUN git clone -b main https://github.com/open-mmlab/mmrazor.git mmrazor \
    && cd mmrazor \
    && pip install --no-cache-dir -e .


# ─────────────────────────────────────────────────────────────────────────────
# OPENMMLAB RUNTIME BACKEND
# ─────────────────────────────────────────────────────────────────────────────
WORKDIR ${BACKEND_PATH}

# onnxruntime
RUN pip install onnxruntime-gpu==1.15.1
RUN wget https://github.com/microsoft/onnxruntime/releases/download/v1.15.1/onnxruntime-linux-x64-gpu-1.15.1.tgz \
    && tar -zxvf onnxruntime-linux-x64-gpu-1.15.1.tgz \
    && rm onnxruntime-linux-x64-gpu-1.15.1.tgz
ENV ONNXRUNTIME_DIR=${BACKEND_PATH}/onnxruntime-linux-x64-gpu-1.15.1
ENV LD_LIBRARY_PATH=${LD_LIBRARY_PATH}:${ONNXRUNTIME_DIR}/lib

# "ppl.cv" 
RUN git clone https://github.com/openppl-public/ppl.cv.git ppl.cv \
    && cd ppl.cv \
    && ./build.sh cuda
ENV PPLCV_DIR=${BACKEND_PATH}/ppl.cv/cuda-build/install/lib/cmake/ppl

# "tensorrt"
ENV TENSORRT_DIR=/opt/tensorrt

# "torchscript"
ENV Torch_DIR=/opt/conda/lib/python3.8/site-packages/torch
ENV LD_LIBRARY_PATH=$Torch_DIR/lib:$LD_LIBRARY_PATH


# ─────────────────────────────────────────────────────────────────────────────
# OPENMMLAB RUNTIME SDK
# ─────────────────────────────────────────────────────────────────────────────
WORKDIR ${OPENMMLAB_PATH}

# "mmdeploy"
RUN git clone --recursive -b v1.2.0 https://github.com/open-mmlab/mmdeploy.git mmdeploy

# "mmdeploy sdk" 
ENV LD_LIBRARY_PATH_BACKUP=$LD_LIBRARY_PATH
ENV LD_LIBRARY_PATH=/usr/local/cuda/compat/lib.real/:$LD_LIBRARY_PATH
ENV MMDEPLOY_SDK_DIR=${OPENMMLAB_PATH}/mmdeploy/build/install

RUN cd ${OPENMMLAB_PATH}/mmdeploy \
    && mkdir -p build \
    && cd build \
    && cmake .. \
        -DCMAKE_CXX_COMPILER=g++ \
        -DMMDEPLOY_BUILD_SDK=ON \
        -DMMDEPLOY_BUILD_SDK_PYTHON_API=ON \
        -DMMDEPLOY_BUILD_EXAMPLES=ON \
        -DMMDEPLOY_TARGET_DEVICES="cuda;cpu" \
        -DMMDEPLOY_TARGET_BACKENDS="ort;trt;torchscript" \
        -DCMAKE_INSTALL_PREFIX=${MMDEPLOY_SDK_DIR} \
        -DMMDEPLOY_CODEBASES="all" \
        -Dpplcv_DIR=${PPLCV_DIR} \
        -DONNXRUNTIME_DIR=${ONNXRUNTIME_DIR} \
        -DTENSORRT_DIR=${TENSORRT_DIR} \
        -DTorch_DIR=${Torch_DIR} \
    && make -j$(nproc) \
    && make install

RUN cd ${OPENMMLAB_PATH}/mmdeploy \
    && pip install --no-cache-dir -e . 

ENV LD_LIBRARY_PATH=${OPENMMLAB_PATH}/mmdeploy/build/lib:${LD_LIBRARY_PATH_BACKUP}
ENV LD_LIBRARY_PATH=${OPENMMLAB_PATH}/mmdeploy/build/install/lib:${LD_LIBRARY_PATH}


# ─────────────────────────────────────────────────────────────────────────────
# WORKSPACE
# ─────────────────────────────────────────────────────────────────────────────
WORKDIR ${WORKSPACE_PATH}
