# ─────────────────────────────────────────────────────────────────────────────
# BASE IMAGE
#   - reference: https://catalog.ngc.nvidia.com/orgs/nvidia/containers/cuda/tags
# ─────────────────────────────────────────────────────────────────────────────
ARG CUDA="11.8.0"
ARG CUDNN="8"
ARG UBUNTU="22.04"

FROM nvcr.io/nvidia/cuda:${CUDA}-cudnn${CUDNN}-devel-ubuntu${UBUNTU}

ENV DEBIAN_FRONTEND=nointeractive
ENV FORCE_CUDA="1"


# # ─────────────────────────────────────────────────────────────────────────────
# # SYSTEM LIBRARIES
# # ─────────────────────────────────────────────────────────────────────────────
# WORKDIR /root/third_party/toolchain

# ARG CMAKE="3.25.2"

# RUN apt-get update \
#     && apt-get install -y --no-install-recommends \
#         apt-utils ca-certificates git vim wget unzip \
#         gcc-11 g++-11 \
#         libopencv-dev libprotobuf-dev protobuf-compiler \
#         python3-dev python3-pip \
#     && rm -rf /var/lib/apt/lists/* \
#     && ln -s /usr/bin/python3 /usr/bin/python 

# # cmake
# RUN wget https://github.com/Kitware/CMake/releases/download/v${CMAKE}/cmake-${CMAKE}-linux-x86_64.tar.gz \
#     && tar -xzvf cmake-*.tar.gz && rm cmake-*.tar.gz \
#     && mv cmake-* cmake
# ENV PATH=/root/third_party/toolchain/cmake/bin:$PATH


# # ─────────────────────────────────────────────────────────────────────────────
# # PYTHON LIBRARIES
# # ─────────────────────────────────────────────────────────────────────────────
# ARG OPENCV="4.5.4.60"
# ARG TORCH="2.1.0"
# ARG TORCHVISION="0.16"

# # torch, torchvision, opencv
# ENV TORCH_CUDA_ARCH_LIST="6.0 6.1 7.0 7.5 8.0 8.6 8.9 9.0+PTX" 
# RUN export CUDA_INT=$(echo $CUDA_VERSION | awk '{split($0, a, "."); print a[1]a[2]}') \
#     && python3 -m pip install --no-cache-dir --upgrade pip setuptools wheel \
#     && python3 -m pip install --no-cache-dir opencv-python==${OPENCV} opencv-python-headless==${OPENCV} opencv-contrib-python==${OPENCV} \
#     && python3 -m pip install --no-cache-dir torch==${TORCH}+cu${CUDA_INT} torchvision==${TORCHVISION}+cu${CUDA_INT} --index-url https://download.pytorch.org/whl/cu118


# # ─────────────────────────────────────────────────────────────────────────────
# # OPENMMLAB CORE
# # ─────────────────────────────────────────────────────────────────────────────
# WORKDIR /root/third_party/openmmlab

# ARG MMENGINE="0.9.0"
# ARG MMCV="2.1.0"

# # mmengine
# RUN git clone -b v${MMENGINE} https://github.com/open-mmlab/mmengine.git mmengine \
#     && cd mmengine \
#     && pip install --no-cache-dir -e .

# # mmcv
# RUN git clone -b v${MMCV} https://github.com/open-mmlab/mmcv.git mmcv \
#     && cd mmcv \
#     && pip install --no-cache-dir -v -e .

# # ─────────────────────────────────────────────────────────────────────────────
# # OPENMMLAB CODEBASE
# # ─────────────────────────────────────────────────────────────────────────────
# WORKDIR /root/third_party/openmmlab/codebase

# ARG MMPRETRAIN="1.1.0"
# ARG MMDETECTION="3.2.0"
# ARG MMSEGMENTATION="1.2.1"
# ARG MMAGIC="1.1.0"
# ARG MMRAZOR="1.0.0"

# # mmpretrain
# RUN git clone -b v${MMPRETRAIN} https://github.com/open-mmlab/mmpretrain.git mmpretrain \
#     && cd mmpretrain \
#     && pip install --no-cache-dir -e .

# # mmdetection
# RUN git clone -b v${MMDETECTION} https://github.com/open-mmlab/mmdetection.git mmdetection \
#     && cd mmdetection \
#     && pip install --no-cache-dir -e .

# # mmsegmentation
# RUN git clone -b v${MMSEGMENTATION} https://github.com/open-mmlab/mmsegmentation.git mmsegmentation \
#     && cd mmsegmentation \
#     && pip install --no-cache-dir -e .

# # mmagic
# RUN git clone -b v${MMAGIC} https://github.com/open-mmlab/mmagic.git mmagic \
#     && cd mmagic \
#     && pip install --no-cache-dir -e .

# # mmrazor
# RUN git clone -b v${MMRAZOR} https://github.com/open-mmlab/mmrazor.git mmrazor \
#     && cd mmrazor \
#     && pip install --no-cache-dir -e .


# # ─────────────────────────────────────────────────────────────────────────────
# # BACKEND LIBRARIES
# # ─────────────────────────────────────────────────────────────────────────────
# WORKDIR /root/third_party/backend

# ARG ONNXRUNTIME="1.15.1"
# # ARG TENSORRT="8.6.1"
# # ARG OPENVINO="2022.3.0"

# # ppl.cv
# RUN git clone https://github.com/openppl-public/ppl.cv.git ppl.cv \
#     && cd ppl.cv \
#     && ./build.sh cuda
# ENV PPLCV_PATH=/root/third_party/backend/ppl.cv/cuda-build/install/lib/cmake/ppl

# # onnxruntime
# RUN wget https://github.com/microsoft/onnxruntime/releases/download/v${ONNXRUNTIME}/onnxruntime-linux-x64-gpu-${ONNXRUNTIME}.tgz \
#     && tar -xzf onnxruntime-*.tgz && rm onnxruntime-*.tgz \
#     && python3 -m pip install --no-cache-dir onnxruntime-gpu==${ONNXRUNTIME}
# ENV ONNXRUNTIME_PATH=/root/third_party/backend/onnxruntime-linux-x64-gpu-${ONNXRUNTIME}
# ENV LD_LIBRARY_PATH=${ONNXRUNTIME_PATH}/lib:${LD_LIBRARY_PATH}

# # tensorrt
# # TODO
# # NVIDIA account login required
# # RUN wget https://github.com/NVIDIA/TensorRT/archive/refs/tags/v{TENSORRT}.tar.gz \
#     # && tar -xzf TensorRT-*.tar.gz && rm TensorRT-*.tgz \
#     # && python3 -m pip install ./python/tensorrt-*-cp${PY_VERSION}-none-linux_x86_64.whl

# # openvino
# # TODO, git-lfs required
# # RUN wget https://github.com/openvinotoolkit/openvino/archive/refs/tags/${OPENVINO}.tar.gz \
#     # && tar -xzf ${OPENVINO}*.tar.gz && rm ${OPENVINO}*.tgz \
#     # && bash openvino-${OPENVINO}/install_openvino_dependencies.sh
#     # && python3 -m pip install --no-cache-dir openvino openvino-dev[onnx]==${OPENVINO} \

# # ncnn
# # TODO

# # ─────────────────────────────────────────────────────────────────────────────
# # OPENMMLAB RUNTIME SDK
# # ─────────────────────────────────────────────────────────────────────────────
# WORKDIR /root/third_party/openmmlab

# ARG MMDEPLOY="1.3.1"

# # "mmdeploy"
# RUN git clone --recursive -b v${MMDEPLOY} https://github.com/open-mmlab/mmdeploy.git mmdeploy

# # "mmdeploy sdk" 
# ENV LD_LIBRARY_PATH_BACKUP=${LD_LIBRARY_PATH}
# ENV LD_LIBRARY_PATH=/usr/local/cuda/compat:${LD_LIBRARY_PATH}
# ENV MMDEPLOY_SDK_DIR=/root/third_party/openmmlab/mmdeploy/build/install

# RUN cd /root/third_party/openmmlab/mmdeploy \
#     && mkdir -p build \
#     && cd build \
#     && cmake .. \
#         -DCMAKE_CXX_COMPILER=g++-11 \
#         -DMMDEPLOY_BUILD_SDK=ON \
#         -DMMDEPLOY_BUILD_SDK_PYTHON_API=ON \
#         -DMMDEPLOY_BUILD_EXAMPLES=ON \
#         -DMMDEPLOY_TARGET_DEVICES="cuda;cpu" \
#         -DMMDEPLOY_TARGET_BACKENDS="ort" \
#         -DCMAKE_INSTALL_PREFIX=${MMDEPLOY_SDK_DIR} \
#         -DMMDEPLOY_CODEBASES="all" \
#         -Dpplcv_DIR=${PPLCV_PATH} \
#         -DONNXRUNTIME_DIR=${ONNXRUNTIME_PATH} \
#     && make -j$(nproc) && make install \
#     && cd .. \
#     && pip install --no-cache-dir -e . 

# ENV LD_LIBRARY_PATH=/root/third_party/openmmlab/mmdeploy/build/lib:${LD_LIBRARY_PATH_BACKUP}
# ENV LD_LIBRARY_PATH=/root/third_party/openmmlab/mmdeploy/build/install/lib:${LD_LIBRARY_PATH}
# ENV PYTHONPATH=/root/third_party/openmmlab/mmdeploy/build/lib:${PHTHONPATH}


# ─────────────────────────────────────────────────────────────────────────────
# WORKSPACE
# ─────────────────────────────────────────────────────────────────────────────
WORKDIR /root/workspace
